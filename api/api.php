<?php

namespace TMAddons\Api;

defined( 'ABSPATH' ) || exit;

class Api {
	public function __construct() {
		include_once( TM_ADDONS_DIR . 'api/class-rest-elementor-templates-controller.php' );
		
		add_action( 'rest_api_init', [ $this, 'templates_routes' ] );
	}

	public function templates_routes() {
		$templates = new REST_Elementor_Templates_Controller();
		$templates->register_routes();
	}
}

new Api();