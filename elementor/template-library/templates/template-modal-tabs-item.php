<?php
/**
 * Template Library Header Tabs
 */

?>
<label>
	<input type="radio" value="{{ slug }}" name="tm-template-modal-header-tab">
	<span>{{ title }}</span>
</label>
