<?php
namespace TMAddons\Elementor\Builder\Conditions;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Archive extends Condition_Base {
	public function get_name() {
		return 'archive';
	}

	public static function get_priority() {
		return 80;
	}

	public function get_label() {
		return __( 'Archives', 'tm-addons' );
	}

	public function get_all_label() {
		return __( 'All Archives', 'tm-addons' );
	}

	public function check( $args ) {
		if ( isset( $args['post_type'] ) ) {
			return is_post_type_archive( $args['post_type'] ) || ( 'post' === $args['post_type'] && is_home() );
		}
		return is_archive() || is_home() || is_search();
	}
}