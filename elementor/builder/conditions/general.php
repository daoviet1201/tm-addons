<?php
namespace TMAddons\Elementor\Builder\Conditions;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class General extends Condition_Base {
	public function get_name() {
		return 'tm-addons';
	}

	public function get_label() {
		return __( 'Entire Site', 'tm-addons' );
	}

	public function get_all_label() {
		return __( 'Entire Site', 'tm-addons' );
	}

	public function check( $args ) {
		return true;
	}
}
